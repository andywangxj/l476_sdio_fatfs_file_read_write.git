/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-07-28     RT-Thread    first version
 */

#include <rtthread.h>

#include <dfs_posix.h> /* 当需要使用文件操作时，需要包含这个头文件 */


#define DBG_TAG "main"
#define DBG_LVL DBG_LOG
#include <rtdbg.h>

int main(void)
{
    int fd, size;
    char s[] = "hello elmfatfs!", buffer[80];
    //----------------------------------------------------

    rt_thread_mdelay(100);      //等待sd_mount线程挂载文件系统，可使用IPC信号量完成同步，提高实时性

    LOG_I("Write string %s to test.txt.\n", s);

    /* 以创建和读写模式打开 /text.txt 文件，如果该文件不存在则创建该文件 */
    fd = open("/text.txt", O_WRONLY | O_CREAT);
    if (fd>= 0)
    {
        write(fd, s, sizeof(s));
        close(fd);
        LOG_I("Write done.\n");
    }

      /* 以只读模式打开 /text.txt 文件 */
    fd = open("/text.txt", O_RDONLY);
    if (fd>= 0)
    {
        size = read(fd, buffer, sizeof(buffer));
        close(fd);
        LOG_I("Read from file test.txt : %s \n", buffer);
        if (size < 0)
            return RT_ERROR;
    }


    return RT_EOK;
}

